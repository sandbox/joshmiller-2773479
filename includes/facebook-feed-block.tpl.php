<?php
/**
 * @file
 * Template for Facebook feed.
 *
 * Copy this file to your theme's directory to override the structure.
 *
 * Provided variables:
 *
 * string $feed
 *   A rendered list of feed items ready to be printed.
 *
 * array $feed_items
 *   A non-rendered list of feed items that could be modified before being
 *   printed.
 *
 * string $feed_title
 *   The title of the page or person of whose feed we are displaying.
 *
 * string $feed_number
 *   The number of likes the page or person has.
 */
?>

<div class="facebook-feed-header">
  <span class="facebook-feed-title"><?php if (!empty($feed_items)): ?>
    <a href="<?php print $feed_items['link']; ?>" rel="external" target="_blank"><?php print $feed_title; ?></a>
  <?php else: ?>
    <?php print $feed_title; ?>
  <?php endif; ?></span>
  <span class="facebook-feed-number"><strong><?php print number_format($feed_number); ?></strong> <?php print variable_get('facebook_feed_stats_title', NULL); ?></span>
</div>
<div class="facebook-feed-content">
  <?php if (!empty($feed_items)): ?>
    <ul>
    <?php foreach ($feed_items as $item) { ?>
      <li>
        <?php if (!empty($item['picture'])): ?>
          <div class="facebook-feed-picture">
            <?php if (!empty($feed_items['link'])): ?>
              <a href="<?php print $feed_items['link']; ?>" rel="external" target="_blank">
            <?php endif; ?>
              <img src="<?php print $item['picture']; ?>" alt="<?php print $feed_title; ?> - <?php t('picture'); ?>" />
            <?php if (!empty($feed_items['link'])): ?>
              </a>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($item['message'])): ?>
          <div class="facebook-feed-message">
            <?php print $item['message']; ?>

            <?php if (!empty($item['date']['formatted'])): ?>
              <div class="facebook-feed-date">
                <?php print $feed_title; ?> &mdash; <?php print $item['date']['formatted']; ?>
              </div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <div class="clear-break"></div>
      </li>
    <?php } ?>
    </ul>
  <?php else: ?>
    <p><em><?php print $feed; ?></em></p>
  <?php endif; ?>
</div>
