# Facebook Feed

A simple module that includes a settings page and one block that will connect to the Facebook API 2.x (tested with
2.0-2.7) and give you a themed feed of the posts on a specific page. This is meant to show a page feed and not a group
or individual's feed. Though, those feeds would be relatively easy to add (patches welcome!)

## Installation &amp; Configuration

To install, you simply copy this module to your module folder and enable the module using drush or the user interface. 
Once enabled, you can configure the module by locating the "Facebook feed" link under the "Content" menu and adding 
your specific app credentials that you will be using. You can easily Google for how to generate a quick Facebook app 
that will give you the credentials you need.

## Placing the block

Once you've added your credentials, all that is left to do is to add the block to your site where you need it. While
Drupal does limit you to one placement on your site, you can achieve more placements if you use Panels or straight php
to call the block as needed.

## Roadmap &amp; similar projects

Once we've got this module approved, we will be maintaining with bug fixes and will entertain new features if the
community wants to contribute. There are many other much more robust facebook modules out there. We just needed a
lightweight version that let us bring in page posts for some of our clients.
